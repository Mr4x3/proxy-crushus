# Python Imports
import os
import uuid
import datetime
import json

# Django Imports
from django.conf import settings
from django.db.models import Q
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils import timezone
from django.utils.html import format_html

# Third Party Django Imports
import requests
from rest_framework.authtoken.models import Token

# Inter App Imports

# Local Imports
from .choices import GENDERS
from .managers import UserManager


def get_user_display_picture_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{0}.{1}'.format(uuid.uuid4(), ext)
    return os.path.join('users', filename)

class User(AbstractBaseUser):
    """
    A Custom User Model.

    As the default user model does not provide the
    ability to identify a user using his email or
    social id, this model overrides django default
    user model providing said functionality.

    Docs: https://docs.djangoproject.com/en/1.10/topics/auth/customizing/
    """

    first_name = models.CharField(max_length=20, blank=True, null=True)
    last_name = models.CharField(max_length=80, blank=True, null=True)
    email = models.EmailField(max_length=255, unique=True, blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDERS, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    display_picture = models.ImageField(upload_to=get_user_display_picture_path, blank=True, null=True)
    registration_midout = models.BooleanField(default=True)
    is_email_verified = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_requester = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def image_url(self):
        if self.display_picture:
            return self.display_picture.url
        return None

    # Name Setters And Getters
    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    @property
    def full_name(self):
        return self.get_full_name()

    @property
    def name(self):
        return self.get_full_name()

    # Token
    def get_auth_token(self):
        """
        Get Or Create Token Key For User.
        """
        if hasattr(self, 'auth_token'):
            key = self.auth_token.generate_key()
            Token.objects.filter(user=self).update(key=key)
            return key
        else:
            token = Token.objects.create(user=self)
            return token.key
