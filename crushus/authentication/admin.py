# Python Imports

# Django Imports
from django.contrib import admin

# Third Party Django Imports

# Inter App Imports

# Local Imports

# Admin For Authentication
# @admin.register(Authentication)
# class AuthenticationAdmin(admin.ModelAdmin):
#     form = AuthenticationForm
#     list_display = ['title', 'description', 'timestamp']
#     list_display_links = None
#     exclude = ('birth_date',)
# readonly_fields = ('address_report',)
# search_fields = ['user__email']
