# Python Imports
import datetime

# Django Imports
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import FormView, ListView, DetailView, TemplateView
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.db.models import F

# Third Party Django Imports

# Inter App Imports

# Local Imports
from .forms import AuthenticationForm
from .models import Authentication


# Views For Authentication
class AddAuthenticationView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """
    View To Add A Authentication.
    """

    model = Authentication
    form_class = AuthenticationForm
    # permission_required = (,)
    template_name = 'authentication/authentication_form.html'
    success_url = reverse_lazy('authentication:authentication_list')
    success_message = 'Authentication Added Successfully!!'


class DetailAuthenticationView(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    """
    View To Get Detail Of A Authentication.
    """

    model = Authentication
    context_object_name = 'authentication'


class ListAuthenticationView(LoginRequiredMixin, ListView):
    """
    List All Authentications.
    """

    model = Authentication
    paginate_by = 10


class UpdateAuthenticationView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
    Update A Authentication's Data.
    """

    model = Authentication
    success_url = reverse_lazy('authentication:authentication_list')
    success_message = 'Authentication Updated Successfully!!'
    fields = ('field',)
    form_class = AuthenticationForm
