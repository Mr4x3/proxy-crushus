# Python Imports

# Django Imports
from django.contrib.auth import authenticate
from django.contrib.gis.geos import Point
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.utils.module_loading import import_string

# Third Party Django Imports
from rest_framework import serializers
from rest_framework import status
from rest_framework.generics import CreateAPIView, UpdateAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets

# Inter App Imports

# Local Imports
from .serializers import UserCreationSerializer, UserSerializer
from crushus.authentication.models import User


class UserCreationAPIView(CreateAPIView):
    """
    Api View To Create User Using Mobile Number.

    Url: /api/auth/v1/create-mobile-user.json
    Method: POST
    Params: name, mobile_number, password, reference_code(op)

    Example: {
        "email": "Vivek@crushus.com",
        "password": "li88nux",
    }
    """

    serializer_class = UserCreationSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.perform_create(serializer)
        # Emails
        response_data = UserSerializer(user).data
        response_data.update({'token': user.get_auth_token()})
        headers = self.get_success_headers(response_data)
        return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.create(serializer.validated_data)



class SetPasswordAPIView(APIView):
    """
    API To Set Password, Essential For Social Loged User

    Url: /api/auth/v1/set-password.json
    Method: POST
    Params: new_password

    Example: {
        "new_password": "1232"
    }
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        new_password = self.request.data.get('new_password', None)
        if new_password:
            self.request.user.set_password(new_password)
            self.request.user.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status.HTTP_406_NOT_ACCEPTABLE)



class UserDetailsAPIView(RetrieveAPIView):
    """
    API to get user data

    Url: /api/auth/v1/user-details.json
    Method: GET
    """

    allowed_methods = ['GET']
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user
