# Python Imports

# Django Imports
from django.conf.urls import url

# Third Party Django Imports
from rest_framework.urlpatterns import format_suffix_patterns

# Inter App Imports
from .views import UserCreationAPIView

# Local Imports


urlpatterns = [
    url(r'^get-in/$', UserCreationAPIView.as_view(), name='get-in'),  # Get In For User
]

# Add Multiple Format Support
urlpatterns = format_suffix_patterns(urlpatterns)
