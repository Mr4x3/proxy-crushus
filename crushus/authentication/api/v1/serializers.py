# Python Imports

# Django Imports

# Third Party Django Imports
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

# Inter App Imports

# Local Imports
from crushus.authentication.models import User

class UserCreationSerializer(serializers.ModelSerializer):
    """
    Serializer to serialize user [Sign Up] creation data.
    """

    class Meta:
        model = User
        fields = ('email', 'password')

    def create(self, validated_data):
        kwargs = {
            'email': validated_data.get('email', None),
            'password': validated_data.get('password', None),
        }
        return User.objects.create_user(**kwargs)


class UserSerializer(serializers.ModelSerializer):
    """
    Serializes User
    """

    class Meta:
        model = User
        fields = ('email',)
        extra_kwargs = {
            'first_name': {'required': False},
        }


class LocationSerializer(serializers.Serializer):
    """
    location update Serializer
    """

    latitude = serializers.FloatField()
    longitude = serializers.FloatField()


class OTPSerializer(serializers.Serializer):
    """
    serializes OTP
    """

    otp = serializers.CharField(max_length=4)


class PasswordUpdateSerializer(serializers.Serializer):
    """
    Get Old Password And New Password And Update To New Password
    """

    current_password = serializers.CharField(max_length=128)
    new_password = serializers.CharField(max_length=128)


class ForgotPasswordOtpVerifySerializer(serializers.Serializer):
    """
    Verify Mobile & Otp
    """

    mobile_number = serializers.CharField(required=True)
    otp = serializers.CharField(max_length=4)
