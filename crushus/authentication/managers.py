"""
Module to provide custom User Manager
"""
# Python Imports

# Django Imports
from django.contrib.auth.models import BaseUserManager

# Third Party Django Imports

# Inter App Imports

# Local Imports


class UserManager(BaseUserManager):

    def normalize_email(self, email):
        """
        Normalize The Email Address By Lowercasing The Domain Part Of It.
        """
        email = email or ''
        try:
            email_name, domain_part = email.strip().rsplit('@', 1)
        except ValueError:
            pass
        else:
            email = '@'.join([email_name, domain_part.lower()])
        return email or None

    def create_user(self, email=None, password=None, **kwargs):
        """
        Creates And Saves A User.
        """
        user = self.model(
            email=self.normalize_email(email),
            **kwargs
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, password, email=None, **kwargs):
        """
        Creates And Saves A Superuser.
        """
        user = self.create_user(
            email=email,
            password=password,
            **kwargs
        )

        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
