# Contains Choices For App Authentication

GENDERS = (
    ('M', 'Male'),
    ('F', 'Female'),
)


REGISTRATION_SOURCES = (
    ('M', 'Mobile'),
    ('F', 'Facebook'),
    ('G', 'Google'),
)


OTP_PURPOSE = (
    ('M', 'Mobile'),
    ('E', 'Email'),
)
