# Python Imports

# Django Imports
from django.conf.urls import url, include

# Third Party Django Imports

# Inter App Imports

# Local Imports

app_name = 'authentication'

# Authentication Views Urls
urlpatterns = [
    # Apis For Authentication App
    url(r'^api/', include('crushus.authentication.api.urls')),  # Authentication Apis
]
