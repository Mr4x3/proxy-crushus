# Python Imports

# Django Imports
from django.apps import AppConfig

# Third Party Django Imports

# Inter App Imports

# Local Imports


class AuthenticationConfig(AppConfig):
    name = 'crushus.authentication'
    verbose_name = "Authentication"

    def ready(self):
        """
        Override this to put in: Users system checks, Users signal registration
        """
        pass
