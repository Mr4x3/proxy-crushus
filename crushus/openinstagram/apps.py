# Python Imports

# Django Imports
from django.apps import AppConfig

# Third Party Django Imports

# Inter App Imports

# Local Imports


class OpeninstagramConfig(AppConfig):
    name = 'crushus.openinstagram'
    verbose_name = "OpenInstagram"

    def ready(self):
        """
        Override this to put in: Users system checks, Users signal registration
        """
        pass
