# Python Imports
import random
import json
from urllib.parse import urlparse

# Django Imports
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.core.cache import cache

# Third Party Django Imports
import requests
from bs4 import BeautifulSoup

# Inter App Imports
from crushus.utils.utils import get_url_key_name, get_relative_url

# Local Imports


# Views For Openinstagram
INSTAGRAM_HOST = 'https://instagram.com'
WEBSITE_HOST = 'https://api.openinstagram.com'
API_OPENINSTAGRAM = 'api.openinstagram.com'

TOP_TAGS = {"love","instagood","photooftheday","beautiful","tbt","happy","cute","fashion","followme","me","follow","like4like","picoftheday","selfie","summer","friends","instadaily","girl","fun","art","repost","smile","instalike","tagsforlikes","food","nature","style","family","igers","likeforlike","nofilter","follow4follow","fitness","amazing","life","travel","vscocam","beauty","bestoftheday","sun","music","beach","followforfollow","sky","photo","vsco","dog","l4l","sunset","f4f","instagram","swag","pretty","hair","ootd","foodporn","makeup","cat","party","girls","cool","lol","tflers","baby","night","funny","model","instapic","motivation","healthy","gym","christmas","yummy","hot","black","pink","blue","work","iphoneonly","photography","instacool","design","flowers","instafood","followback","fit","webstagram","my","instafollow","blackandwhite","wedding","work","tweegram","iphonesia","winter","instalove","instasize","home","sweet","nyc","red","birthday","handmade","lifestyle","drawing","throwback","goodmorning","london","holiday","awesome","igdaily","blessed","morning","inspiration","sea","new","happiness","likes","nice","clouds","puppy","look","tattoo","coffee","green","white","dinner","weekend","shopping","eyes","fitfam","wcw","nails","sunday","boy","paris","instago","latepost","day","delicious","loveit","yum","relax","instagramhub","good","dress","breakfast","shoes","all_shots","italy","japan","foodie","snow","bored","instagramers","instafashion","artist","nike","best","spring","vacation","health","onedirection","catsofinstagram","landscape","architecture","halloween","dubai","goodnight","bodybuilding","bestfriend","vintage","colorful","memories","tb","instaphoto","trip","chanel","2016","chocolate","goodtimes","outfit","mcm","flower","friend","indonesia","usa","training","live","adorable","newyork","loveher","picture","photographer","cake","friday","201","cats","peace","anime","sketch","perfect","pet","likeforfollow","bff","likesforlikes","20likes","shoutout","bestfriends","daily","water","amor","happybirthday","moda","saturday","city","lovely","gay","dance","quote","illustration","sister","dogs","luxury","fitspo","tumblr","blonde","color","bomdia","pic","eatclean","autumn","istanbul","mylove","beer","fashionista","view","boanoite","jj","likeback","forever","2015","picstitch","kids","california","i","vegan","insta","truth","thailand","sunshine","crazy","fresh","light","uae","instamoment","latergram","disney","wanderlust","1","kuwait","quotes","adventure","homemade","miami","top","gucci","dior","portrait","lovehim","throwbackthursday","instacollage","france","school","sunrise","travelgram","la","sisters","canon","photoshoot","boyfriend","prada","car","harrystyles","instadog","onlineshop","the","fashionblogger","fall","tree","street","diet","loveyou","painting","gorgeous","москва","sale","hermes","instatravel","goals","holidays","jewelry","football","familia","sunny","colors","yoga","ocean","animal","today","cold","ink","kitty","friendship","blogger","sport","natural","time","likes4likes","weightloss","hiphop","stylish","artwork","nailart","nikon","followher","australia","abs","tattoos","draw","mylife","babygirl","world","cleaneating","niallhoran","kpop","you","adidas","kiss","cutie","mexico","running","a","animals","strong","polishgirl","brother","dogstagram","gold","iloveyou","instachile","dessert","handsome","2014","trees","pets","justinbieber","russia","tired","wine","fendi","s4s","gopro","mountains","starbucks","germany","body","potd","lovethem","spain","mood","bahrain","couple","givenchy","losangeles","heart","enjoy","traveling","barcelona","boys","feliz","louistomlinson","muscle","monday","chicago","amigos","iphone","jakarta","florida","ksa","photogrid","exercise","brasil","sundayfunday","with","turkey","yolo","venezuela","goodvibes","streetart","2","mua","vegas","uk","roadtrip","creative","lfl","makeupartist","regrann","crossfit","louisvuitton","inlove","instafit","followhim","face","pizza","streetstyle","explore","rain","moscow","merrychristmas","true","and","celine","liampayne","valentino","drinks","burberry","princess","bali","icecream","yellow","cartier","canada","petstagram","gift","summertime","foodgasm","weather","like4follow","pleasefollow","2017","chill","diy","cousins","graffiti","cardio","garden","followers","healthyfood","instahub","gymlife","in","mom","train","shop","100happydays","teamo","lake","follows","run","kitten","teamfollowback","chloe","eat","amo","passion","orange","catstagram","zaynmalik","man","familytime","interiordesign","old","comment","lips","haha","exo","followall","god","bajumurah","rock","cafe"}

TOP_USERS = TOP_TAGS


class HomeDummyPageView(TemplateView):
    """
    Dummy Home Page View
    """

    template_name = "openinstagram/dummy_home_page.html"
    content_type = "text/plain"


def get_user_json(user_name, max_id=None):
    """
    Get Json Data From Official Instagram Pages
    Some Parts inpired By https://github.com/midnightSuyama/instagram-explore
    """

    get_url = INSTAGRAM_HOST + '/' + user_name
    payload = {'__a': '1'}
    if max_id is not None:
        payload['max_id'] = max_id
    try:
        user_json = requests.get(get_url, params=payload).json()
        # cursor = user_json['user']['media']['page_info']['end_cursor']
        return user_json
    except:
        pass
    return '404'


def get_tag_json(tag_name, max_id=None):
    url = INSTAGRAM_HOST + "/explore/tags/%s/" % tag_name
    payload = {'__a': '1'}
    if max_id is not None:
        payload['max_id'] = max_id

    try:
        tag_json = requests.get(url, params=payload).json()
        # cursor = res['tag']['media']['page_info']['end_cursor']
        return tag_json
    except:
        pass
    return '404'


def JsonFromOfficialMedia(path, count=3, page_no=0):
    """
    [Depricated]
    Get Json From Media
        path: Mostly Username
        count: ?count=3, Means 3 Items Per Page
        page_no: ?page_no=4, Page No. 4
    """

    recieved_data = requests.get(INSTAGRAM_HOST + '/' + path + '/media')
    result_dict = dict()
    result_dict['next_page'] = 'https://api.openinstagram.com/{}/?count={}&page_no={}'.format(path, count, page_no+1)
    if recieved_data:
        dict_data = recieved_data.json()['items']
        data_parts = len(dict_data)/count
        # Pages Parts Should Be Greater Then Page_No
        if not page_no:
            result_dict['items'] = dict_data[0:count]
            return result_dict
        if page_no > data_parts:
            return None
        if page_no + 1 > data_parts:
            result_dict['items'] = dict_data[page_no * count:]
        else:
            result_dict['items'] = dict_data[page_no * count: (page_no + 1) * count]
        return result_dict


def JsonFromNearlyOfficial(path, query):
    """
    Get Json Data From Official Instagram Pages
    """

    get_url = INSTAGRAM_HOST + path + '?__a=1'
    if query:
        get_url = get_url + '&?' + query
    recieved_data = requests.get(get_url, None)
    if recieved_data:
        return recieved_data.json()
    return '404'


class OpenInstagramHomeView(TemplateView):
    """
    HOME PAGE
    Throws 100 Top Instagram Accounts
    """

    template_name = 'openinstagram/home_page.html'

    def get(self, request, *args, **kwargs):
        url_path = get_relative_url(request)
        cache_key = get_url_key_name(url_path)
        result_dict = cache.get(cache_key)
        if not result_dict:
            num_to_select = 25
            # requested_data = requests.get('https://socialblade.com/instagram/top/100/followers').text  # Access Instagram Tracking Site
            # soup = BeautifulSoup(requested_data, "html.parser")
            # grams = soup.find_all('div', class_="table-cell")  # Scrapes The Content From The Table On The Page

            # top100 = []  # Produces Top100 List
            # for g in grams:  # Populates Top 100 List
            #     a = g.find("a")
            #     if a is not None:
            #         top100.append(a.get_text())
            # Result Of Above  Commented Code
            top100_users = {'instagram', 'selenagomez', 'arianagrande', 'taylorswift', 'beyonce', 'cristiano', 'kimkardashian', 'kyliejenner', 'justinbieber', 'therock', 'kendalljenner', 'nickiminaj', 'natgeo', 'neymarjr', 'nike', 'leomessi', 'khloekardashian', 'mileycyrus', 'jlo', 'katyperry', 'ddlovato', 'kourtneykardash', 'victoriassecret', 'badgalriri', 'kevinhart4real', 'fcbarcelona', 'realmadrid', 'theellenshow', 'justintimberlake', 'zendaya', 'caradelevingne', '9gag', 'chrisbrownofficial', 'vindiesel', 'shakira', 'champagnepapi', 'davidbeckham', 'gigihadid', 'emmawatson', 'jamesrodriguez10', 'kingjames', 'garethbale11', 'adele', 'nikefootball', 'zacefron', 'vanessahudgens', 'iamzlatanibrahimovic', 'ladygaga', 'maluma', 'nasa', 'nba', 'ronaldinho', 'danbilzerian', 'luissuarez9', 'zayn', 'shawnmendes', 'chanelofficial', 'hm', 'adidasfootball', 'brumarquezine', 'harrystyles', 'ayutingting92', 'letthelordbewithyou', 'adidasoriginals', 'hudabeauty', 'anitta', 'niallhoran', 'camerondallas', 'zara', 'marinaruybarbosa', 'zachking', 'karimbenzema', 'lucyhale', 'princessyahrini', 'nickyjampr', 'andresiniesta8', 'raffinagita1717', 'onedirection', 'manchesterunited', 'krisjenner', 'natgeotravel', 'marcelotwelve', 'snoopdogg', 'deepikapadukone', 'louisvuitton', 'lelepons', 'priyankachopra', 'shaymitchell', 'davidluiz_4', 'ashleybenson', 'sr4oficial', 'kalbiminrozeti', 'prillylatuconsina96', 'britneyspears', 'leonardodicaprio', 'jbalvin', 'laudyacynthiabella', 'ciara', 'stephencurry30', 'instagrambrasil'}
            top100_users = random.sample(top100_users, 16)
            user_profile_url = "http://api.openinstagram.com/{}"
            top_users = []
            for top_user in top100_users:
                user = requests.get(user_profile_url.format(top_user)).json()['user']
                top_users.append({'full_name':user['full_name'], 'username': user['username'], 'top_user_image': user['profile_pic_url_hd'], 'followed_by': user['followed_by']['count'], 'media': user['media']['count']})
            top_tags = random.sample(TOP_TAGS, num_to_select)
            result_dict = {'urls': top100_users}
            result_dict['top_tags'] = top_tags
            result_dict['top_users'] = top_users
            cache.set(cache_key,result_dict)
        if request.get_host() == API_OPENINSTAGRAM:
            # https://api.openinstagram.com
            return HttpResponse(json.dumps(result_dict), content_type='application/json')
        else:
            return self.render_to_response({'result_dict': result_dict})


class GetProfileDataView(TemplateView):
    """
    [Depricated]
    Get Data From User With Using Media
    allowed_method: ['GET']
    Example: https://api.openinstagram.com/the.vivek/?count=3&page_no=2
    """

    def get(self, *args, **kwargs):
        username = kwargs['username']
        page_no = int(self.request.GET.get('page_no', 0))
        count = int(self.request.GET.get('count', 3))
        result_dict = JsonFromOfficialMedia(username, count, page_no)
        return HttpResponse(json.dumps(result_dict), content_type='application/json')


class GetUserProfileAPIView(TemplateView):
    """
    User Profile Details
    allowed_method: ['GET']
    """

    template_name = 'openinstagram/user_profile.html'

    def get(self, request, *args, **kwargs):
        url_path = get_relative_url(request)
        cache_key = get_url_key_name(url_path)
        result_dict = cache.get(cache_key)
        if not result_dict:
            user_name = kwargs['username']
            max_id = self.request.GET.get('max_id', None)
            result_dict = get_user_json(user_name, max_id)
            cache.set(cache_key,result_dict)
        if request.get_host() == API_OPENINSTAGRAM:
            # https://api.openinstagram.com
            return HttpResponse(json.dumps(result_dict), content_type='application/json')
        else:
            return self.render_to_response({'result_dict': result_dict})


class GetTagAPIView(TemplateView):
    """
    Get Tags
    allowed_method: ['GET']
    """

    template_name = 'openinstagram/tag_images.html'

    def get(self, request, *args, **kwargs):
        url_path = get_relative_url(request)
        cache_key = get_url_key_name(url_path)
        result_dict = cache.get(cache_key)
        if not result_dict:
            tag = kwargs['tag']
            max_id = self.request.GET.get('max_id', None)
            result_dict = get_tag_json(tag, max_id)
            cache.set(cache_key,result_dict)
        if request.get_host() == API_OPENINSTAGRAM:
            # https://api.openinstagram.com
            return HttpResponse(json.dumps(result_dict), content_type='application/json')
        else:
            return self.render_to_response({'result_dict': result_dict})


class GetMediaAPIView(TemplateView):
    """
    Get Media
    allowed_method: ['GET']
    """

    template_name = 'openinstagram/media_images.html'

    def get(self, request, *args, **kwargs):
        url_path = get_relative_url(request)
        cache_key = get_url_key_name(url_path)
        result_dict = cache.get(cache_key)
        if not result_dict:
            media_code = kwargs['media_code']
            get_url = INSTAGRAM_HOST + "/p/%s/?__a=1" % media_code
            try:
                result_dict = requests.get(get_url).json()
            except:
                result_dict = {'detail': 404}
            cache.set(cache_key, result_dict)
        if request.get_host() == API_OPENINSTAGRAM:
            # https://api.openinstagram.com
            return HttpResponse(json.dumps(result_dict), content_type='application/json')
        else:
            return self.render_to_response({'result_dict': result_dict})


class CatchAllAPIView(TemplateView):
    """
    If EveryThing Fails Then This API Will F**king Handle It
    allowed_method: ['GET']
    """

    def get(self, *args, **kwargs):
        path_after_domain_name = self.request.get_full_path()
        query_string = urlparse(path_after_domain_name).query
        path = urlparse(path_after_domain_name).path
        result_dict = JsonFromNearlyOfficial(path, query_string)
        return HttpResponse(json.dumps(result_dict), content_type='application/json')


class SearchAPIView(TemplateView):
    """
    Search User & Tags N Places.
    """

    def get(self, *args, **kwargs):
        query_string = self.request.GET.get('query', None)
        if query_string:
            instagram_search = 'https://www.instagram.com/web/search/topsearch/?query={}'.format(query_string)
            result_dict = requests.get(instagram_search).json()
            return HttpResponse(json.dumps(result_dict), content_type='application/json')
        else:
            return HttpResponse(json.dumps({}), content_type='application/json')


class GetRandomTags(TemplateView):
    """
    Give Number Of Random Tags
    """

    def get(self, *args, **kwargs):
        num_to_select = self.kwargs.get('random_tags', 1)
        result_dict = dict()
        random_tags = random.sample(TOP_TAGS, int(num_to_select))
        result_dict['random_tags'] = random_tags
        return HttpResponse(json.dumps(result_dict), content_type='application/json')


class GetRandomUsers(TemplateView):
    """
    Give Random Number Of Users
    """

    def get(self, *args, **kwargs):
        num_to_select = self.kwargs.get('random_users', 1)
        result_dict = dict()
        random_users = random.sample(TOP_USERS, int(num_to_select))
        result_dict['random_users'] = random_users
        return HttpResponse(json.dumps(result_dict), content_type='application/json')
