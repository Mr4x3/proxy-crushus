# Python Imports

# Django Imports
from django.conf.urls import url
from django.views.decorators.cache import cache_page

# Third Party Django Imports

# Inter App Imports

# Local Imports
from .views import HomeDummyPageView, OpenInstagramHomeView, GetUserProfileAPIView, GetTagAPIView, GetMediaAPIView, CatchAllAPIView, SearchAPIView, GetRandomTags, GetRandomUsers

app_name = 'openinstagram'

URL_CACHE_EXPIRE_IN = 86400
# Openinstagram Views Urls
urlpatterns = [
    # Openinstagram Urls
    url(r'^$', HomeDummyPageView.as_view(), name='home_dummy_page'),  # OpenInstagram Dummy HomePage JustSimple.
    url(r'^home-page$', OpenInstagramHomeView.as_view(), name='home_page'),  # OpenInstagram HomePage.
    url(r'^get-random-tags/(?P<random_tags>[a-zA-Z0-9_.-]+)$', GetRandomTags.as_view(), name='get-random-tags'),  # Get Random Tags.
    url(r'^get-random-users/(?P<random_users>[a-zA-Z0-9_.-]+)$', GetRandomUsers.as_view(), name='get-random-users'),  # Get Random Users.
    url(r'^search$', SearchAPIView.as_view(), name='search'),  # Get Search Data For API.
    url(r'^(?P<username>[a-zA-Z0-9_.-]+)$', GetUserProfileAPIView.as_view(), name='user_profile_data'),  # Get Data For Profile Page.
    url(r'^explore/tags/(?P<tag>[a-zA-Z0-9_.-]+)/$', GetTagAPIView.as_view(), name='tag_data'),  # Get Data For Profile Page.
    url(r'^p/(?P<media_code>[a-zA-Z0-9_.-]+)/$', GetMediaAPIView.as_view(), name='tag_data'),  # Get Data For Profile Page.
    # url(r'^', CatchAllAPIView.as_view(), name='catch_all_api'),  # Main API Which Catch All API.
]
