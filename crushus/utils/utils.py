# Python Imports
import hashlib

# Django Imports

# Third Party Django Imports

# Inter App Imports

# Local Imports

# Utility Functions For The Projects


def get_url_key_name(url):
    """
    Convert A Specific Url To Hash, To Be Used As Key For Storing And Cashing Its Content.
    """

    url_hash = hashlib.sha256()
    url_hash.update(url.encode('utf-8'))
    return "hash_" + url_hash.hexdigest()


def get_relative_url(request):
    """
    Get address After Domain or sub-domain Name
    Ex.: https://proxy.crushus.com/thepiratebay.org
    Return: thepiratebay.org
    """

    slash = request.build_absolute_uri().find("/", len(request.scheme + "://"))
    if slash == -1:
        return "/"
    return request.build_absolute_uri()[slash + 1:]
