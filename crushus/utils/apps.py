# Python Imports

# Django Imports
from django.apps import AppConfig

# Third Party Django Imports

# Inter App Imports

# Local Imports


class UtilsConfig(AppConfig):
    name = 'crushus.utils'
    verbose_name = "Utils"

    def ready(self):
        """
        Override this to put in: Users system checks, Users signal registration
        """
        pass
