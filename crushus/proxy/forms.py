# Python Imports

# Django Imports
from django.contrib.auth import authenticate
from django import forms
from django.core.validators import RegexValidator
from django.contrib.auth.models import User, AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

# Third Party Django Imports

# Inter App Imports

# Local Imports
from .models import Proxy


# Forms For Proxy
class ProxyForm(forms.ModelForm):
    """
    Create A Proxy Form.
    """

    class Meta:
        model = Proxy
        fields = ('field',)
        widgets = {
            'field': forms.Textarea,
        }

    def __init__(self, *args, **kwargs):
        pass
