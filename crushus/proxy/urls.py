# Python Imports

# Django Imports
from django.conf.urls import url
from django.views.decorators.cache import cache_page
from django.conf import settings

# Third Party Django Imports

# Inter App Imports

# Local Imports
from .views import HomePageView, MirrorHandler, MostCompatibleWebsitesView, CleanupView, IndexedView, DisclaimerView, FeaturesView

app_name = 'proxy'

# Proxy Views Urls
urlpatterns = [
    url(r'^$', cache_page(settings.CLEAR_CACHE_IN_TIME)(HomePageView.as_view()), name='proxy_home'),  # Proxy Home Page
    url(r'^most-compatible-websites/', cache_page(settings.CLEAR_CACHE_IN_TIME)(MostCompatibleWebsitesView.as_view()), name='most_compatible_websites'),  # Website List Which Are Most Compatible And Tested
    url(r'^clean/', CleanupView.as_view(), name='clean'),  # View Which Delete Database Tables For Recently Visited & More.
    url(r'^indexed/$', cache_page(settings.CLEAR_CACHE_IN_TIME)(IndexedView.as_view()), name='indexed'),  # Recently Indexed Pages
    url(r'^disclaimer/$', cache_page(settings.CLEAR_CACHE_IN_TIME)(DisclaimerView.as_view()), name='disclaimer'),  # Disclaimer For Website
    url(r'^features/$', cache_page(settings.CLEAR_CACHE_IN_TIME)(FeaturesView.as_view()), name='features'),  # Features For Website
    url(r'^', MirrorHandler.as_view(), name='mirror_handler'),  # Proxy Handler [Main View]
]
