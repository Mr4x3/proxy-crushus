# Python Imports

# Django Imports
from django.shortcuts import redirect
from django.contrib.gis.geoip2 import GeoIP2
from django.conf import settings

# Third Party Django Imports

# Inter App Imports

# Local Imports


class RedirectToWebsiteMixin(object):
    """
    Redirect To Some Website If Given Condition Satifies
    """

    def this(self, request, ip, *args, **kwargs):
        """
        If This Happen Then Redirect To That
        """
        gip2 = GeoIP2()
        try:
            country_code = gip2.country(ip)['country_code']
        except:
            country_code = 'IN'
        if country_code == 'US' or country_code == 'CA':
            return True
        else:
            return False

    def than_that(self, request, *args, **kwargs):
        return 'http://web.crushus.com/where_to_go.html'

    def dispatch(self, request, *args, **kwargs):
        if settings.BUSSINESS:
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[-1].strip()
            else:
                ip = request.META.get('REMOTE_ADDR', None)
            if self.this(request, ip):
                return redirect(self.than_that(request))
        return super(RedirectToWebsiteMixin, self).dispatch(request, *args, **kwargs)
