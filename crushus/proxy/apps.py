# Python Imports

# Django Imports
from django.apps import AppConfig

# Third Party Django Imports

# Inter App Imports

# Local Imports


class ProxyConfig(AppConfig):
    name = 'crushus.proxy'
    verbose_name = "Proxy"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
