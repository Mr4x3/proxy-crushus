# Python Imports

# Django Imports
from django.db import models

# Third Party Django Imports

# Inter App Imports

# Local Imports


# Models For Proxy App
class EntryPointModel(models.Model):
    """
    Database Model For Proxy.
    """

    key_name = models.CharField(max_length=128)
    translated_address = models.CharField(max_length=2083)
    display_address = models.CharField(max_length=100, null=True)
    last_updated = models.DateTimeField(auto_now=True)
    # page_title = db.TextProperty()
    # clicks = db.IntegerProperty(default=0)

    def __str__(self):
        return self.translated_address


class MostCompatibleWebsitesModel(models.Model):
    """
    Model For Storing Most Compatible Websites Which Can be browsed anytime
    """

    website_title = models.CharField(max_length=100)
    domain_name = models.CharField(max_length=200)
    priority = models.IntegerField()
    is_active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.website_title
