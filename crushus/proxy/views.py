# Python Imports
import re
import hashlib
import logging

# Django Imports
from django.views.generic import TemplateView, ListView
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.cache import cache
from django.conf import settings
from django.shortcuts import redirect

# Third Party Imports
import requests

# Inter App Imports
from crushus.proxy import transform_content
from crushus.proxy.mixins import RedirectToWebsiteMixin
from crushus.utils.utils import get_url_key_name, get_relative_url

# Local Imports
from .models import EntryPointModel, MostCompatibleWebsitesModel

# Get Ads Block From Web File :]
GET_ADS = cache.get_or_set('get_ads', requests.get('http://web.crushus.com/ads').text, 300)
GET_ADS_PAGE_FRONT = cache.get_or_set('get_ads_page_front', requests.get('http://web.crushus.com/AdsFiles/ads_front').text, 300)
GET_ADS_PAGE_TOP = cache.get_or_set('get_ads_page_top', requests.get('http://web.crushus.com/AdsFiles/ads_top').text, 300)
GET_ADS_PAGE_BOTTOM = cache.get_or_set('get_ads_page_bottom', requests.get('http://web.crushus.com/AdsFiles/ads_bottom').text, 300)

EXPIRATION_DELTA_SECONDS = 60

HTTP_PREFIX = "http://"

HTTPS_PREFIX = "https://"

IGNORE_HEADERS = frozenset([
    "Content-Type",  # Added As We Are Using Django content_type
    "set-cookie",
    "expires",
    "cache-control",
    # Ignore hop-by-hop headers
    "connection",
    "keep-alive",
    "proxy-authenticate",
    "proxy-authorization",
    "te",
    "trailers",
    "transfer-encoding",
    "upgrade",
])

TRANSFORMED_CONTENT_TYPES = frozenset([
    "text/html",
    "text/css",
])

MAX_CONTENT_SIZE = 10 ** 6

top_replace = re.compile('<!DOCTYPE html.*>', flags=re.I)
bottom_replace = re.compile('</body>\n</html>', flags=re.I)


def go_404(request):
    """
    View For Redirecting 404 Page
    """
    return redirect('/')


class MirroredContent(object):
    """
    Heart Class Where Megic Starts For Creating And Mirroring Content Of Other Websites.
    """

    def __init__(self, original_address, translated_address, status, headers, data, base_url):
        # Create A Simple Mirrored Website Object Which Can Be Used Further
        self.original_address = original_address
        self.translated_address = translated_address
        self.status = status
        self.headers = headers
        self.data = data
        self.base_url = base_url

    @staticmethod
    def get_by_key_name(key_name):
        return cache.get(key_name, None)

    @staticmethod
    def get_headers_by_key_name(key_name):
        return cache.get(key_name + 'headers', None)

    @staticmethod
    def fetch_and_store(key_name, base_url, translated_address, mirrored_url, request):
        """
        Fetch If Not In Cache & And Cache A Page If It Is Html Or Css Else Let It Fetch From Source.

        Args:
            key_name: Hash to use to store the cached page.
            base_url: The hostname of the page that's being mirrored.
            translated_address: The URL of the mirrored page on this site.
            mirrored_url: The URL of the original page. Hostname should match the base_url.

        Returns:
            A New HttpResponse Object Converted With Links In Website, If The Page Was Successfully Retrieved.
            Http404 If Any Errors Occurred Or The Content Could Not Be Retrieved.
        """

        response = cache.get(key_name)
        if response:
            return response

        fetch_headers = {
            'User-Agent': 'Mr.4x3 Powered',
        }
        try:
            response = requests.get(mirrored_url, headers=fetch_headers)
            # Contabo Dont Return 404 {
            if response.url == 'https://contabo.com/':
                raise Http404
            # }
        except:
            # return None
            raise Http404

        content_encoding = response.encoding
        # Headers Of Retrived Url
        content_type = response.headers.get("Content-Type", None)
        if content_encoding:
            # If the transformed content is over 1MB, truncate it (yikes!)
            content = response.text[:MAX_CONTENT_SIZE]
            # Split For text/html or text/css
            content_type = content_type.split(';')[0].lower() if content_type else 'application/x-binary'
            if content_type in TRANSFORMED_CONTENT_TYPES:
                # This is Where F**king Processing Happens, Make It Fast And & Everything Work Fine.
                content = transform_content.TransformContent(base_url, mirrored_url, content)
                # Store the entry point down here, once we know the request is good and
                # there has been a cache miss (i.e., the page expired). If the referrer
                # wasn't local, or it was '/', then this is an entry point.
                if request.META.get('HTTP_REFERER', None) == request.scheme + '://' + request.get_host() + '/':
                    EntryPointModel.objects.create(key_name=key_name, translated_address=translated_address)
                if 'text/html' in content_type:
                    content = top_replace.sub('<!DOCTYPE html> \n<div> {ads_top} </div>'.format(ads_top=GET_ADS_PAGE_TOP), content)
                    content = bottom_replace.sub('<br/> {ads_bottom} </body>\n</html>'.format(ads_bottom=GET_ADS_PAGE_BOTTOM), content)
            response = HttpResponse(content, content_type=content_type)
            cache.set(key_name, response)
        else:
            response = HttpResponse(response.content, content_type=content_type)
        return response


class BaseView(RedirectToWebsiteMixin, TemplateView):
    """
    A View By Which Other Related Classes Can Inherit
    """

    def get_base_url(self):
        # Get base address After / domain or sub-domain Name Example: goo.cox
        slash = self.request.path.find("/", 1)
        if slash == -1:
            return self.request.path[1:]
        return self.request.path[1:slash]

    def is_recursive_request(self):
        # Need A Fix
        user_agent = self.request.META.get('HTTP_USER_AGENT', None)
        if user_agent and 'Mr.4x3 Powered' in user_agent:
            # logging.warning("Ignoring recursive request by user-agent=%r; ignoring")
            raise Http404
        return False


class HomePageView(BaseView):
    """
    Home Page For Website
    """

    template_name = 'proxy/main.html'

    def get(self, request, *args, **kwargs):
        if self.is_recursive_request():
            return
        latest_urls = EntryPointModel.objects.all().order_by("last_updated")[:25]
        not_latest_urls = EntryPointModel.objects.all().order_by("-last_updated")[:25]

        secure_url = None
        if self.request.scheme == "http":
            secure_url = "https://%s%s" % (self.request.get_host(), self.request.path)

        context = {
          "ads_block": GET_ADS_PAGE_FRONT,
          "secure_url": secure_url,
          "latest_urls": latest_urls,
          "not_latest_urls": not_latest_urls,
        }
        return self.render_to_response(context)


class MirrorHandler(BaseView):
    """
    Proxy Handle Mirroring From Given Url
    """

    def get(self, request, *args, **kwargs):
        base_url = self.get_base_url()
        if self.is_recursive_request():
            return

        if self.request.path.endswith("favicon.ico"):
            return HttpResponseRedirect('/static/favicon.ico')
        assert base_url
        translated_address = get_relative_url(request)
        mirrored_url = HTTP_PREFIX + translated_address

        # Use sha256 hash instead of mirrored url for the key name, since key
        # names can only be 500 bytes in length; URLs may be up to 2KB.
        cache_key = get_url_key_name(mirrored_url)

        return MirroredContent.fetch_and_store(cache_key, base_url, translated_address, mirrored_url, request)


class CleanupView(TemplateView):
    """
    Cleans Up Entrypoint In Database Records.
    """

    def get(self, *args, **kwargs):
        try:
            EntryPointModel.objects.all().delete()
            message = 'Done'
        except:
            message = "Problem Can't Clean"
        return HttpResponse(message)


class IndexedView(BaseView):
    """
    Give All Indexed Pages Recently
    """

    template_name = 'proxy/indexed.html'

    def get(self, *args, **kwargs):
        secure_url = None
        if self.request.scheme == "http":
            secure_url = "https://%s%s" % (self.request.get_host(), self.request.path)
        latest_urls = EntryPointModel.objects.all().order_by("last_updated")[:100]
        context = {
          "latest_urls": latest_urls,
          "secure_url": secure_url,
        }
        return self.render_to_response(context)


class MostCompatibleWebsitesView(ListView):
    """
    View For Showing Most Compatible Websites
    """

    context_object_name = 'compatible_urls'
    queryset = MostCompatibleWebsitesModel.objects.filter(is_active=True)
    paginate_by = 500
    ordering = 'priority'


class DisclaimerView(TemplateView):
    """
    Give Disclaimer For Website
    """

    template_name = 'proxy/disclaimer.html'


class FeaturesView(TemplateView):
    """
    Shows Features In Website
    """

    template_name = 'proxy/features.html'
