# Python Imports

# Django Imports
from django.contrib import admin

# Third Party Django Imports

# Inter App Imports

# Local Imports
from .models import EntryPointModel, MostCompatibleWebsitesModel


# Admin For Proxy
@admin.register(EntryPointModel)
class EntryPointAdmin(admin.ModelAdmin):
    pass


@admin.register(MostCompatibleWebsitesModel)
class MostCompatibleWebsitesAdmin(admin.ModelAdmin):
    pass
