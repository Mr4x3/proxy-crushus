sudo apt install -y update && sudo apt install -y upgrade
sudo apt-get install -y ca-certificates supervisor
# curl -fsSL https://yum.dockerproject.org/gpg | sudo apt-key add -
# sudo apt-get install software-properties-common
wget -qO- https://get.docker.com/ | sh
# sudo add-apt-repository  "deb https://apt.dockerproject.org/repo/ ubuntu-$(lsb_release -cs) main"
# sudo apt-get update
# sudo apt-get -y install docker-engine
# sudo systemctl status docker
sudo usermod -aG docker $(whoami)
# Add Your Packages From Here
sudo apt install -y python3-pip
sudo pip3 install docker-compose
sudo pip3 install glances
# sudo cp utility/proxy-crushus.conf /etc/supervisor/conf.d/proxy-crushus.conf
# sudo supervisorctl reread
# sudo supervisorctl start proxy-crushus
# sudo supervisorctl status
chmod 777 staticfiles
sudo docker-compose build
# sudo docker-compose run django python manage.py migrate
# sudo docker-compose run django python manage.py loaddata most_compatible_websites.json
sudo docker-compose up -d
# sudo docker-compose run django python manage.py createsuperuser
