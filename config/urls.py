"""
crushus.com URL Configuration

The `urlpatterns` List Routes Urls To Views.

"""
# Python Imports

# Django Imports
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView
from django.views import defaults as default_views
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.conf.urls import handler404

# Third Party Django Imports

# Inter App Imports

# Local Imports


admin.site.site_header = admin.site.site_title = 'Crushus'

# Error Pages
handler404 = 'crushus.proxy.views.go_404'

# Main Urls For Website
urlpatterns = [
    url(r'^admin/', admin.site.urls),  # Default Admin
    url(r'^sitemap\.xml$', cache_page(settings.CLEAR_CACHE_IN_TIME)(TemplateView.as_view(template_name='proxy/sitemap.xml', content_type='text/xml')), name='sitemap_xml'),  # Sitemap For SearchEngine
    url(r'^robots.txt$', cache_page(settings.CLEAR_CACHE_IN_TIME)(TemplateView.as_view(template_name="proxy/robots.txt", content_type="text/plain")), name='robots_txt'),  # Robots.txt
    url(r'^favicon.ico$', RedirectView.as_view(url=staticfiles_storage.url('favicon.ico'), permanent=False), name="favicon_ico"),  # Favicon.ico
    url(r'^', include('crushus.proxy.urls')),  # Proxy Main App

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
