#!/bin/sh
python /app/oi_manage.py collectstatic --noinput
python /app/oi_manage.py migrate
/usr/local/bin/gunicorn config.settings.oi.wsgi -w 4 -b 0.0.0.0:5002 --chdir=/app
