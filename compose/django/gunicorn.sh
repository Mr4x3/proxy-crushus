#!/bin/sh
python /app/manage.py collectstatic --noinput
python /app/manage.py migrate
/usr/local/bin/gunicorn config.wsgi -w 12 -b 0.0.0.0:5001 --chdir=/app
