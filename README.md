# Proxy Crushus #

### About ###

Crushus is built with [Python 3.5][0] using the [Django Web Framework 1.10][1] Live Can be Found At [CrushUs Proxy][2]. This Project is _Used for creating Alternate Server Where We Have No Restrictions of bandwidth or Censorship._

This project has the following basic apps:

* crushus/proxy/ (_App For Transforming Data And Show All Data_)
* crushus/authentication/ (_App For Authenticate Using API_)

### History ###

* Version 1 Was a Fork From `Brett Slatkin's` Project On Python 2.5 [Works On Google Appspot] In Year 2010.
* Version 1.7 Was Migrated To Python 2.7 With Many Bugs On Google Appspot Year 2015.
* Version 2 Was Fully Migrated To Python 2.7 On Google Appspot In Year 2017 Feb.
* Version 3 With Nearly All Bugs fixed On Python 2.7 In Year 2017 Feb.
* Version 4 [Alpha] Was Created With Python 3.5 And Django 1.10 In Year 2017 Feb 24.
* Version 4 Was Created With Python 3.5 And Django 1.10 In Year 2017 April 17.
* Version 5 [Alpha] Was Created, No Contamination Of Openinstagram With Python 3.5 And Django 1.10 In Year 2017 May 17.
* Version 5 [RC] Was Involved Fusion Of 2 Branches[Projects] proxy & api.openinstagram, On Year 2017 May 20. 


### Projects Branches ###

* crushus Now Hold Things For [https://proxy.crushus.com]
* appspot_py27 Used For Goolge Appspot For [https://proxy.crushus.com]
* [DEP] proxy was Used For [https://proxy.crushus.com] With Nginx.
* [DEP] nginx_out Used For [https://proxy.crushus.com] With No Nginx On Docker.
* [DEP] openinstagram was Used For [https://api.openinstagram.com].


### Notes ###

* This Project Breath Only In Internet, Make sure You Have That.
* This project will be using postgres as it has low memory footprint and it does Not Do Much Data Query.
* Cache [Redis] Is used When data Not Changing Frequently.
* Uses GEOIP2 Install Via:
    `sudo add-apt-repository ppa:maxmind/ppa`
    `sudo apt update`
    `sudo apt install libmaxminddb0 libmaxminddb-dev mmdb-bin`

* Use redis Use Server File For Redis /etc/redis/6379.conf
* As Of 7 April 2017, This Thing Runs Now On 4 Core Xeon So Dont Try To run on simple Machine [like single core It will burn]
* New Project Structure Created, So Many Websites Can Be In There.

## Installation

### Setup Development Environment [All]
    `wget -qO- https://get.docker.com/ | sh`
    `sudo apt install -y python3-pip`
    `sudo pip3 install docker-compose`

### Development Proxy [https://proxy.crushus.com]
    `sudo docker-compose -f dev.yml up --build`

### Development OpenInstagram [https://openinstagram.com]
    `sudo docker-compose -f oi_dev.yml up --build`

### Build The Fucking Server With [Know What R U Doin. Fuck...]
    `sudo ./install.sh`

### For Production [Docker]
    `sudo docker-compose build`
    `sudo docker-compose up -d`
    `Or`
    `sudo docker-compose up --build`

### Database Migrations [Docker]
    `sudo docker-compose -f dev.yml run django python manage.py makemigrations`
    `sudo docker-compose -f dev.yml run django python manage.py migrate`

### Get Data Dump Most Compatible Websites [Might Not Need This]
    `sudo docker-compose run django python manage.py dumpdata proxy.MostCompatibleWebsitesModel --indent 4 > most_compatible_websites.json`
    `sudo docker-compose run django python manage.py createsuperuser`
    `sudo docker-compose run django python manage.py loaddata most_compatible_websites.json`

### Scripts
    `sudo docker-compose -f dev.yml run django python manage.py shell`
    `for i, value in enumerate(lis):`
        MostCompatibleWebsitesModel.objects.create(website_title=value.capitalize(),domain_name=value, priority=i+18, is_active=True)
    `sudo docker-compose -f dev.yml run django python manage.py dumpdata proxy.MostCompatibleWebsitesModel --indent 4 > most_compatible_websites.json`

### Clean Docker [Docker]
    `sudo docker stop $(sudo docker ps -a -q)`
    `sudo docker rm $(sudo docker ps -a -q)`

### Quick Start [This Sucks [Old It Might Not Work], Use Development]

To set up a development environment quickly, first install Python 3. It
comes with virtualenv built-in. So create a virtual env by:

    1. `$ python3 -m venv crushus`
    2. `$ . crushus/bin/activate`
    3. `$ create database crushus`

Install all dependencies:

    pip install -r requirements.txt

Run migrations:

    python manage.py migrate

### Detailed instructions

Take a look at the docs for more information.
SideNote [I Dont Think It Exists]

[0]: https://www.python.org/
[1]: https://www.djangoproject.com/
[2]: https://proxy.crushus.com
